class Api::V1::BooksController < ActionController::Base
  def list
    render json: 20.times.map{|i| {id: i+1, name: "book#{i+1}"}}
  end
end
