import Immutable from 'immutable';

import actionTypes from '../constants';
import { combineReducers } from 'redux'

export const $$initialState = Immutable.fromJS({
  name: '', // this is the default state that would be used if one were not passed into the store
});

function bookReducer(state = {}, action) {
  switch (action.type) {
    case actionTypes.BOOK_LIST:
      return Object.assign({}, state, {
        books: action.books
      })
    default:
      return state
  }
}

const rootReducer = combineReducers({
  bookReducer
})

export default rootReducer
