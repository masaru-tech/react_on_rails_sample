import React from 'react';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';

import configureStore from '../store/configureStore';

import routes from '../routes/routes';

export default (props, location) => {

  const store = configureStore(browserHistory, props);

  return (
    <Provider store={store}>
      <Router history={browserHistory} children={routes} />
    </Provider>
  );
};
