import React from 'react';
import { Provider } from 'react-redux';
import { match, RouterContext } from 'react-router';

import configureStore from '../store/configureStore';
import routes from '../routes/routes';

import { UPDATE_LOCATION } from 'react-router-redux';

import createHistory from 'react-router/lib/createMemoryHistory';

export default (props, location) => {
  const history = createHistory(location);
  const store = configureStore(history, props);

  let error;
  let redirectLocation;
  let routeProps;

  match({ routes, location }, (_error, _redirectLocation, _routeProps) => {
    error = _error;
    redirectLocation = _redirectLocation;
    routeProps = _routeProps;
  });

  if (error || redirectLocation) {
    return { error, redirectLocation };
  }

  return (
    <Provider store={store}>
      <RouterContext {...routeProps} />
    </Provider>
  );
};
