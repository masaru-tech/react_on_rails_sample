import React, { PropTypes } from 'react';
import DetailComponent from '../components/Detail';
import { connect } from 'react-redux';

function mapStateToProps(state) {
  return {};
}

class Detail extends React.Component {
  render() {
    return (
      <DetailComponent />
    );
  }
}

export default connect(mapStateToProps)(Detail);
