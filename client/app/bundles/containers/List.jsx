import React, { PropTypes } from 'react';
import ListComponent from '../components/List';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions';

function mapStateToProps(state) {
  const { bookReducer } = state.rootReducer
  return {
    books: bookReducer.books || []
  }
}

function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(Actions, dispatch),
  };
}

class List extends React.Component {
  componentDidMount() {
    const { getList } = this.props;
    getList();
  }

  render() {
    return (
      <ListComponent {...this.props} />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);
