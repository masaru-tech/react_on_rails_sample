import React from 'react';
import { Route, IndexRoute } from 'react-router';
import List from '../containers/List';
import Detail from '../containers/Detail';

export default (
  <Route path="/books">
    <Route path="list" component={List} />
    <Route path=":id/detail" component={Detail} />
  </Route>
);
