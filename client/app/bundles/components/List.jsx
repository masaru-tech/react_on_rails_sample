import React, { PropTypes } from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

class ListItem extends React.Component {
  constructor(props) {
    super(props);
    this.push = this.push.bind(this);
  }

  push() {
    this.props.transitionTo(`/books/${this.props.book.id}/detail`)
  }

  render() {
    return (<ListGroupItem onClick={this.push}>{this.props.book.name}</ListGroupItem>)
  }
}

export default class List extends React.Component {
  render() {
    const list = this.props.books.map((book) => { return(<ListItem book={book} key={book.id} transitionTo={this.props.transitionTo} />) });
    return (
      <ListGroup>
        {list}
      </ListGroup>
    )
  }
}
