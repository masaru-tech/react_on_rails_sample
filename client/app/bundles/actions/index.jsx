import actionTypes from '../constants';
import { routeActions } from 'react-router-redux';
import axios from 'axios';

export function updateName(name) {
  return {
    type: actionTypes.HELLO_WORLD_NAME_UPDATE,
    name,
  };
}

export function receiveList(books) {
  return {
    type: actionTypes.BOOK_LIST,
    books: books
  }
}

export function transitionTo(url) {
  return dispatch => {
    dispatch(routeActions.push(url))
  }
}

export function getList() {
  return dispatch => {
    axios.get('/api/v1/books/list')
    .then(function (response) {
      dispatch(receiveList(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}
