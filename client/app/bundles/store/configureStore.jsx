import { compose, createStore, applyMiddleware, combineReducers } from 'redux';

import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';

import rootReducer from '../reducers';

import { syncHistory, routeReducer } from 'react-router-redux';

export default (history, props) => {
  const reduxRouterMiddleware = syncHistory(history);
  const middleware = [thunkMiddleware, loggerMiddleware(), reduxRouterMiddleware];

  // Redux expects to initialize the store using an Object, not an Immutable.Map
  const initialState = {};

  const composedStore = compose(
    applyMiddleware(...middleware)
  );
  const reducers = combineReducers({
    rootReducer,
    routing: routeReducer
  })
  const storeCreator = composedStore(createStore);
  const store = storeCreator(reducers, initialState);

  return store;
};
