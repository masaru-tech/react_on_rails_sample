import mirrorCreator from 'mirror-creator';

const actionTypes = mirrorCreator([
  'HELLO_WORLD_NAME_UPDATE',
  'BOOK_LIST'
]);

export default actionTypes;
